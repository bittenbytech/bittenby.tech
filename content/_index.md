# Hi! I'm Simon

Throughout the years, I've been called a sysadmin, devops engineer, SRE, 
magician, rockstar, architect, tech lead, and a big nerd. I'm just curious which 
buzzword is next.
