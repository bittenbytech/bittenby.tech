+++
title = 'Always Dns'
date = 2024-01-31T10:12:24+01:00
draft = false
+++

*It's always DNS.*

The paradigm holds true. While I usually use Cloudflare for my DNS needs, I was 
lazy last night and figured I'd just do it through my domain registrar. One 
service less to mess around in and set things up.

I should've just used Cloudflare. The registrar messed up the auto-generated SOA 
record, meaning I spent ages for it to clear from global DNS caches.

I ended switching the entire thing over to Cloudflare anyways.

Lesson learned: don't skip steps because it's late and you *just want it done*.
