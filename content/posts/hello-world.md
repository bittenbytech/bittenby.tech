+++
title = 'Hello World'
date = 2024-01-30T21:26:15+01:00
draft = false
+++

## Hi

Time for a personal space. After working for damn near a decade behind closed 
doors, I miss partaking in the tech community, showcasing the things I play 
with. I've read enough blogs to know there's no point in saying what you can 
expect on here.

I'm Simon. I'm bitten by tech. I have no idea what I'm doing and I'm rolling 
with it.

## The blog tech

Behind the scenes, this is hosted on [Gitlab Pages][1], generated with [Hugo][2] 
and the [Risotto theme][3].

[1]: https://docs.gitlab.com/ee/user/project/pages/
[2]: https://hugo.io
[3]: https://themes.gohugo.io/themes/risotto/
